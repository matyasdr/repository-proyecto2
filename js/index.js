$(function(){
		$("[data-bs-toggle="tooltip"]").tooltip();
		$("[data-bs-toggle="popover"]").popover();
		$(".carousel").carousel({
				interval: 2000
		});
		
		/* Suscripciones de eventos */
		$("#contacto").on("show.bs.modal", function(e){
			console.log('El modal se está mostrando');
			
			$("#contactoBtn").removeClass("btn-outline-success");
			$("#contactoBtn").addClass("btn-primary");
			$("#contactoBtn").prop("disabled", true);
		});						
		$("#contacto").on("shown.bs.modal", function(e){
			console.log("El modal se mostro");
		});
		$("#contacto").on("hide.bs.modal", function(e){
			console.log("El modal se oculta");
		});
		$("#contacto").on("hidden.bs.modal", function(e){
			console.log("El modal se ocultó");
		});		